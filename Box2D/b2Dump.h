#include "Box2D.h"

class b2Dump {
public:

	static void DumpDynamicTree(b2World* world);
	static void DumpDynamicTree(b2DynamicTree* tree);
	static void DumpTreeNode(b2TreeNode* node,int32 nodeId,int32 tab,int32 level);
	static void DumpContact(b2Contact* c);
	static int DumpTabLevel;
	static char DumpTag[128];
};
