#include <stdio.h>
#include "b2Dump.h"

FILE* b2Fp = freopen("b2log.txt", "w", stdout);
b2DynamicTree* tempTree = NULL;
char linebuf[1024*1024];
char linetab[1024];
char cnum[8];
int32 treeHeight = 0;
char treechar[65536];
int32 treeNx = 200;

int b2Dump::DumpTabLevel = 0;
char b2Dump::DumpTag[128];
#define DUMPTAB {for(int i = 0;i<b2Dump::DumpTabLevel;i++) printf("\t");}

void b2Dump::DumpDynamicTree(b2World* world)
{
	tempTree = &world->m_contactManager.m_broadPhase.m_tree;
	DumpDynamicTree(tempTree);
}

void b2Dump::DumpDynamicTree(b2DynamicTree* tree)
{
	printf("---------------DumpDynamicTree Dump Start-----------------------\n");
	treeHeight = tree->GetHeight();
	memset(linebuf, 0, 1024 * 1024);
	b2Dump::DumpTreeNode(&tree->m_nodes[tree->m_root], tree->m_root,tree->GetHeight(),0);
	for (int i = 0;i <= treeHeight;i++) {
		char* nline = &linebuf[i * 1024];
		int slen = strlen(nline);
		nline[slen] = '\n';
		printf(nline);
	}
	printf("==================DumpDynamicTree Dump End=======================\n");
}
void b2Dump::DumpTreeNode(b2TreeNode* node,int32 nodeId,int32 tab,int32 level) {
	char* line = &linebuf[1024 * level];int32 ic = 0;
	for (int32 i = 0;i < 1024;i++) {
		if (ic == tab) {
			sprintf(cnum, "%d",nodeId);
			int32 dlen = strlen(cnum);
			memcpy(line + i + dlen, line + i, 1024 - (i + dlen));
			memcpy(line + i, cnum, dlen);
			break;
		}
		if (line[i] == '\t') ic++; else if(line[i]==0){line[i] = '\t';ic++;}
	}
	{
		if (node->child1 != b2_nullNode)
			b2Dump::DumpTreeNode(&tempTree->m_nodes[node->child1], node->child1, tab-1,level+1);
		if (node->child2 != b2_nullNode)
			b2Dump::DumpTreeNode(&tempTree->m_nodes[node->child2], node->child2, tab+1,level+1);
	}
}
void b2Dump::DumpContact(b2Contact* c)
{
	DUMPTAB;printf("%sb2Contact[%d] m_flags:%d\n",b2Dump::DumpTag,size_t(c), c?c->m_flags:0);
}